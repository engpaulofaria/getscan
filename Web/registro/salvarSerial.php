<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>
<body style="padding:10px">
<?php

function validaEmail($email){
	//verifica se e-mail esta no formato correto de escrita
	if (!preg_match('^([a-zA-Z0-9.-])*([@])([a-z0-9]).([a-z]{2,3})^',$email)){
		return false;
    }
    else{
		//Valida o dominio
		$dominio=explode('@',$email);
		if(!checkdnsrr($dominio[1],'A')){
			return false;
		}
		else{return true;} // Retorno true para indicar que o e-mail é valido
	}
}

function exibeMsg($msg, $tipo = "alert-danger"){
echo "<div class=\"alert $tipo\" role=\"alert\">
	  <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
	  <span class=\"sr-only\">Error:</span>
	  " .  $msg . "</div>";
}

function enviaEmail($to, $subject, $message){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'From: GetScan <serial@getscan.com.br>' . "\r\n";

	// Mail it
	mail($to, $subject, $message, $headers);
}

$email = trim($_POST['email']);
$computerId = trim($_POST['computerId']);
if($email=="" || $computerId==""){
	exibeMsg("Os dados obrigat&oacute;rios devem ser preenchidos!");	
} else {
	if(validaEmail($email)){
		require "SerialHelper.php";
		$serial = SerialHelper::gerarSerial($computerId);
		$conn = mysql_connect("localhost", "root", "");
		mysql_select_db("dbgetscan",$conn);

		//verifica se o email e o computer id já foi registrado
		$sql = "select * from tb_serial where email = '$email' and computerId = '$computerId'";
		$res = mysql_query($sql,$conn);
		$row = mysql_fetch_row($res);
		//var_dump($row); die();
		if(!$row){ //nao achou registro
			$sqlInsert = "insert into tb_serial (email, computerId, serial) values ('$email','$computerId','$serial')";
			mysql_query($sqlInsert, $conn);
			$html = "<p>Ol&aacute;! <br>Segue o serial que dever&aacute; ser informado no aplicativo agente:</p>
			<h1 style=\"color: #8a6d3b; background-color: #fcf8e3; border-color: #faebcc; padding:10px;\">$serial</h1>
			<p>Obrigado por registrar o produto!<br>Equipe GetScan</p>";
			enviaEmail($email, "Registro do GetScan",$html);
			//die($html);
			exibeMsg("O serial foi enviado para seu e-mail. Obrigado por registrar!" , "alert-success");
		} else {
			exibeMsg("O serial <b>{$row[3]}</b> j&aacute; havia sido enviado para seu e-mail!" , "alert-success");
			
		}

	} else{
		exibeMsg("O email informado n&atilde;o &eacute; v&aacute;lido");
	}
}
?>
</body>
</html>