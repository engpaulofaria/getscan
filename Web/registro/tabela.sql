CREATE TABLE `tb_serial` (  
	`id` int(11) NOT NULL AUTO_INCREMENT,  
	`email` varchar(80) NOT NULL,  
	`computerId` varchar(80) NOT NULL,  
	`serial` varchar(80) NOT NULL,  
	`dataCadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1