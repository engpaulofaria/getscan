<html>
<head> 

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<link rel='stylesheet' id='kleo-plugins-css'  href='http://playanywhere.com.br/getscan/wp-content/themes/kleo/assets/css/plugins.min.css?ver=2.1' type='text/css' media='all' />

<link rel='stylesheet' id='kleo-app-css'  href='http://playanywhere.com.br/getscan/wp-content/themes/kleo/assets/css/app.min.css?ver=2.1' type='text/css' media='all' />

</head>
<body style="padding:10px">


<div class="wrap-content">
					
<div class="alert alert-info" role="alert">Para gerar seu serial para o GetScan, informe seu e-mail e o Identificador da M&aacute;quina apresentado na tela de registo do m&oacute;dulo agente.</div>


   
    <div>
        <div class="wpcf7" >
<form name="" action="salvarSerial.php" method="post" class="wpcf7-form">
	<p>Seu e-mail* <br>
		<span class="wpcf7-form-control-wrap your-email">
			<input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" required="required" maxlength="80">
		</span> 
	</p>
	
	<p>Identifica&ccedil;&atilde;o da m&aacute;quina(Computer-ID)*<br>
		<span class="wpcf7-form-control-wrap your-subject">
			<input type="text" name="computerId" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" required="required" maxlength="80" pattern=".{32,}">
		</span> 
	</p>
	
	<p>
		<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit">
		<img class="ajax-loader" src="http://playanywhere.com.br/getscan/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Enviando ..." style="visibility: hidden;">
	</p>
</form></div>
            </div><!--end article-content-->

    
<!-- End  Article -->

        

				
				</div>
</body>
</html>