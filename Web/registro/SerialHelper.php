<?php

class SerialHelper{
	private static function calculateSecurityHash($data, $algo){
		return hash($algo, $data, false);
	}

	public static function gerarSerial($computerId){
		$serialNumberEncoded = self::calculateSecurityHash($computerId, "md2")
							. self::calculateSecurityHash($computerId, "md5")
							. self::calculateSecurityHash($computerId, "sha1");
		$serial = "" . $serialNumberEncoded[32]
							. $serialNumberEncoded[76]
							. $serialNumberEncoded[100]
							. $serialNumberEncoded[50] . "-"
							. $serialNumberEncoded[2]
							. $serialNumberEncoded[91]
							. $serialNumberEncoded[73]
							. $serialNumberEncoded[72]
							. $serialNumberEncoded[98] . "-"
							. $serialNumberEncoded[47]
							. $serialNumberEncoded[65]
							. $serialNumberEncoded[18]
							. $serialNumberEncoded[85] . "-"
							. $serialNumberEncoded[27]
							. $serialNumberEncoded[53]
							. $serialNumberEncoded[102]
							. $serialNumberEncoded[15]
							. $serialNumberEncoded[99];
		return $serial;					
	}
	
	public static function validarSerial($computerId, $serial){
		return $serial === self::gerarSerial($computerId);
	}

}

?>