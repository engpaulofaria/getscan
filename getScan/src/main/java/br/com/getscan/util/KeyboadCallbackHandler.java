package br.com.getscan.util;

import java.awt.AWTException;

import br.com.getscan.app.ApplicationContext;
import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.enums.AcaoFinalLinha;
import br.com.getscan.app.vo.Config;

public class KeyboadCallbackHandler implements ICallbackHandler {
	private static Keyboard keyboard = null;
	
	public void processarMensagem(String msg) throws Exception {
		Thread.sleep(2000);
		System.out.println(msg);
		getKeyboard().type(msg);
		Config config = ((Config) ApplicationContext
				.getInstance().getAtribute(
						ConstantesGerais.CONFIGURACOES_GERAIS));
		if(!config.getAcaoFinalLinha().equals(AcaoFinalLinha.NENHUMA)){
			getKeyboard().type(config.getAcaoFinalLinha().getAcao());
		}
	}
	
	private Keyboard getKeyboard() throws AWTException{
		if(keyboard == null){
			keyboard = new Keyboard();
		}
		return keyboard;
	}

}
