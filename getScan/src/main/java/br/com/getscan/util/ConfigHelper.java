package br.com.getscan.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.Properties;

import org.apache.log4j.Logger;

import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.enums.AcaoFinalLinha;
import br.com.getscan.app.enums.ModoOperacaoEnum;
import br.com.getscan.app.vo.Config;

public class ConfigHelper {

	final static Logger LOGGER = Logger.getLogger(ConfigHelper.class);

	private final static String PROP_MODO_OPERACAO = "MODO_OPERACAO";
	private final static String PROP_ACAO_FINAL_LINHA = "ACAO_FINAL_LINHA";
	private final static String PROP_CAMINHO_SERVIDOR = "CAMINHO_SERVIDOR";
	private final static String PROP_PORTA_AGENTE = "PORTA_AGENTE";
	private final static String PROP_IDENTIFICACAO = "IDENTIFICACAO";
	private static final String PROP_PORTA_SERVIDOR = "PORTA_SERVIDOR";
	private static final String PROP_COMPUTER_ID = "COMPUTER_ID";
	private static final String PROP_CHAVE_ACESSO = "CHAVE_ACESSO";

	public static Config carregarConfig() throws Exception {
		InputStream in;
		Config config;
		try {
			File configFile = new File("config.properties");
			String caminhoConfig = configFile.getAbsolutePath();

			LOGGER.debug("Carregando config: " + caminhoConfig);

			in = new FileInputStream(caminhoConfig);
			Properties prop = new Properties();
			prop.load(in);
			config = new Config();
			config.setModoOperacao(ModoOperacaoEnum
					.getModoOperacaoByCodigo(Long.parseLong(prop
							.getProperty(PROP_MODO_OPERACAO))));
			String hostname = InetAddress.getLocalHost().getHostName()
					.toUpperCase();
			config.setAcaoFinalLinha(AcaoFinalLinha
					.getAcaoFinalLinhaByCodigo(Long.parseLong(prop
							.getProperty(PROP_ACAO_FINAL_LINHA))));
			config.setComputerId(prop.getProperty(PROP_COMPUTER_ID));
			config.setChaveAcesso(prop.getProperty(PROP_CHAVE_ACESSO));
			if (config.getModoOperacao().equals(ModoOperacaoEnum.LOCAL)) {
				if (!prop.containsKey(PROP_PORTA_AGENTE)) {
					config.setPortaAgente(ConstantesGerais.DEFAULT_LOCAL_SERVER_SOCKET_PORT);
				} else {
					config.setPortaAgente(Integer.parseInt(prop
							.getProperty(PROP_PORTA_AGENTE)));
				}
				config.setIdentificacao(hostname);
			} else if (config.getModoOperacao().equals(
					ModoOperacaoEnum.STANDALONE)) {
				if (!prop.containsKey(PROP_PORTA_AGENTE)) {
					config.setPortaAgente(ConstantesGerais.DEFAULT_LOCAL_SERVER_SOCKET_PORT);
				} else {
					config.setPortaAgente(Integer.parseInt(prop
							.getProperty(PROP_PORTA_AGENTE)));
				}
				config.setIdentificacao(hostname);
			} else if (config.getModoOperacao().equals(
					ModoOperacaoEnum.STANDALONE)) {
				config.setCaminhoServidor(prop
						.getProperty(PROP_CAMINHO_SERVIDOR));
				config.setPortaServidor(Integer.parseInt(prop
						.getProperty(PROP_PORTA_SERVIDOR)));
			} else if (config.getModoOperacao().equals(ModoOperacaoEnum.CLOUD)) {
				config.setCaminhoServidor(ConstantesGerais.URL_SERVIDOR_CLOUD);
				config.setIdentificacao(prop.getProperty(PROP_IDENTIFICACAO));
			}
		} catch (Exception e) {
			throw new Exception("Erro ao carregar as configurações.", e);
		}
		return config;
	}

	public static void main(String[] args) {
		try {
			carregarConfig();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * Properties prop = new Properties(); OutputStream output = null;
		 * 
		 * try {
		 * 
		 * output = new FileOutputStream("config.properties");
		 * 
		 * // set the properties value prop.setProperty("database",
		 * "localhost"); prop.setProperty("dbuser", "mkyong");
		 * prop.setProperty("dbpassword", "password");
		 * 
		 * // save properties to project root folder prop.store(output, null);
		 * 
		 * } catch (IOException io) { io.printStackTrace(); } finally { if
		 * (output != null) { try { output.close(); } catch (IOException e) {
		 * e.printStackTrace(); } }
		 * 
		 * }
		 */
	}

	public static void salvarConfig(Config novaConfig) throws Exception {
		Properties prop = new Properties();
		OutputStream output = null;

		try {
			File configFile = new File("config.properties");
			output = new FileOutputStream(configFile);

			prop.setProperty(PROP_MODO_OPERACAO, novaConfig.getModoOperacao()
					.getCodigo().toString());
			prop.setProperty(PROP_PORTA_AGENTE,
					String.valueOf(novaConfig.getPortaAgente()));
			prop.setProperty(PROP_ACAO_FINAL_LINHA, novaConfig
					.getAcaoFinalLinha().getCodigo().toString());
			prop.setProperty(PROP_COMPUTER_ID, novaConfig.getComputerId());
			prop.setProperty(PROP_CHAVE_ACESSO, novaConfig.getChaveAcesso());
			prop.store(output, null);

		} catch (IOException io) {
			throw new Exception("Erro ao salvar as configurações.", io);
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}
}
