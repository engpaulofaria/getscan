package br.com.getscan.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ComputerIDUtil {
	
	private static String getMacAddress()
	        throws UnknownHostException, SocketException {
	    InetAddress addr = InetAddress.getLocalHost();
	    NetworkInterface ni = NetworkInterface.getByInetAddress(addr);
	    if (ni == null)
	        return null;

	    byte[] mac = ni.getHardwareAddress();
	    if (mac == null)
	        return null;

	    StringBuilder sb = new StringBuilder(18);
	    for (byte b : mac) {
	        if (sb.length() > 0)
	            sb.append(':');
	        sb.append(String.format("%02x", b));
	    }
	    return sb.toString();
	}
	
	public static String getComputerID(){
		try {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			//byte[] mac = network.getHardwareAddress();
			//String sn = getSerialNumber("C");
			//String cpuId = getMotherboardSN();
			String serial = getMacAddress();
			return DigestUtil.gerarMD5(serial);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
