package br.com.getscan.util;

public interface ICallbackHandler {

	public void processarMensagem(String msg) throws Exception;
	
}
