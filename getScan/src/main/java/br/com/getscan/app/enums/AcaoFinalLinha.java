package br.com.getscan.app.enums;

public enum AcaoFinalLinha {
	NENHUMA(0L, ""),
	ENTER(1L,"\n"),
	TAB(2L, "\t");
	
	private Long codigo;
	private String acao;
	
	private AcaoFinalLinha(Long codigo, String acao){
		this.codigo = codigo; 
		this.acao = acao;
	}
	
	public Long getCodigo(){
		return codigo;
	}
	
	public String getAcao(){
		return acao;
	}
	
	public static AcaoFinalLinha getAcaoFinalLinhaByCodigo(Long codigo){
		for (AcaoFinalLinha modo : AcaoFinalLinha.values()) {
			if(modo.getCodigo().equals(codigo)){
				return modo;
			}
		}
		return null;
	}
	
}
