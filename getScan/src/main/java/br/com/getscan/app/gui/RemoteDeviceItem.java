package br.com.getscan.app.gui;

import java.io.IOException;
import java.io.Serializable;

import javax.bluetooth.RemoteDevice;

public class RemoteDeviceItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RemoteDevice device; 

	protected RemoteDeviceItem(RemoteDevice device ) {
		this.device = device;
	}
	
	@Override
	public String toString() {
		try {
			return device.getFriendlyName(false);
		} catch (IOException e) {
			return super.toString();
		}
	}
	
	public RemoteDevice getDevice(){
		return device;
	};

}
