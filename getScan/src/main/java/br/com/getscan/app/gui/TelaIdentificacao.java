package br.com.getscan.app.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;
import java.lang.reflect.Method;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JPanel;

import br.com.getscan.app.ApplicationContext;
import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.vo.Config;

import java.awt.FlowLayout;
import java.awt.event.WindowEvent;

public class TelaIdentificacao extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TelaIdentificacao() {

		setAlwaysOnTop(true);
		setTitle("Identificação");

		centralizarTela();

		setUndecorated(true);
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setResizable(false);
		setSize(Toolkit.getDefaultToolkit().getScreenSize().getSize().width,
				Toolkit.getDefaultToolkit().getScreenSize().getSize().height);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setOpaque(false);

		Config config = ((Config) ApplicationContext.getInstance().getAtribute(
				ConstantesGerais.CONFIGURACOES_GERAIS));

		JLabel lblIdentificacao = new JLabel(config.getIdentificacao());
		lblIdentificacao.setFont(new Font("Tahoma", Font.BOLD, 65));
		lblIdentificacao.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblIdentificacao, BorderLayout.CENTER);

		pack();
	}

	private void centralizarTela() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);

	}

	@Override
	public void setVisible(boolean b) {
		super.setVisible(b);
		java.awt.EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(3000);
					TelaIdentificacao.this
							.dispatchEvent(new WindowEvent(
									TelaIdentificacao.this,
									WindowEvent.WINDOW_CLOSING));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}

	public void setOpaque(boolean opaque) {
		Window window = (Window) this;
		String version = System.getProperty("java.runtime.version");
		if (version.startsWith("1.7")) {
			window.setBackground(new Color(0, 0, 0, 0));
		} else {
			try {
				Class<?> awtUtilsClass = Class
						.forName("com.sun.awt.AWTUtilities");
				if (awtUtilsClass != null) {

					Method method = awtUtilsClass.getMethod("setWindowOpaque",
							Window.class, boolean.class);
					method.invoke(null, window, opaque);

				}
			} catch (Exception exp) {
				exp.printStackTrace();
			}
		}
	}

}
