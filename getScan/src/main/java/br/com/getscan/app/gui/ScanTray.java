package br.com.getscan.app.gui;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ScanTray implements MouseListener, ActionListener{
	
	private static ScanTray instance = null;
	
	private MenuItem menuItemSair;
	private MenuItem menuConfigurar;
	private MenuItem menuSobre;
	private MenuItem menuIdentificacao;
	
	private PopupMenu popup;
	
	//Telas
	private TelaConfiguracoes telaConfiguracoes;
	private TelaSobre telaSobre;
	private TelaIdentificacao telaIdentificacao;
	
	public MenuItem getMenuIdentificacao() {
		if(menuIdentificacao == null){
			menuIdentificacao = new MenuItem("Identificação do agente...");
			menuIdentificacao.addActionListener(this);
		}
		return menuIdentificacao;
	}
	
	public MenuItem getMenuItemSair() {
		if(menuItemSair == null){
			menuItemSair = new MenuItem("Sair");
			menuItemSair.addActionListener(this);
		}
		return menuItemSair;
	}

	public void setMenuItemSair(MenuItem menuItemSair) {
		this.menuItemSair = menuItemSair;
	}

	private ScanTray(){
		
	}
	
	public static ScanTray getInstance(){
		if(instance == null){
			instance = new ScanTray();
		}
		return instance;
	}

	public void getTray() throws Exception{
		

		// Aqui vamos testar se o recurso é suportado
		if (SystemTray.isSupported()) {
			final TrayIcon trayIcon;
			URL url = ScanTray.class.getResource("/images/barcode-icon.png");
			Image image = Toolkit.getDefaultToolkit().getImage(url);
			SystemTray tray = SystemTray.getSystemTray();

			
			// criando um objeto do tipo TrayIcon
			trayIcon = new TrayIcon(image, "GetScan Agente", getPopup());
			trayIcon.setImageAutoSize(true);

			// Seguida adicionamos os actions listeners
			//trayIcon.addActionListener();
			trayIcon.addMouseListener(this);

			try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				throw new Exception("Erro aoo adicionar o TrayIcon");
			}
		} else {

			// Caso o item System Tray não for suportado
			throw new Exception("Recurso ainda não esta disponível pra o seu sistema");
			

		}
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		//System.out.println("Tray Icon - O Mouse foi pressionado!");		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private void actionSair() {
		JOptionPane.showMessageDialog(null, "O GetScan Agente será encerrado!");
		System.exit(0);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getMenuItemSair())){
			actionSair();
		} else if(e.getSource().equals(getMenuConfigurar())){
			actionConfigurar();
		} else if(e.getSource().equals(getMenuSobre())){
			actionSobre();
		} else if(e.getSource().equals(getMenuIdentificacao())){
			actionIdentificacao();
		}
		
	}

	private void actionIdentificacao() {
		// TODO Auto-generated method stub
		getTelaIdentificacao().setVisible(true);
		//getTelaIdentificacao().pack();
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		getTelaIdentificacao().setVisible(false);
		
	}

	private void actionSobre() {
		getTelaSobre().setVisible(true);
		
	}

	private JFrame getTelaSobre() {
		if(telaSobre == null){
			telaSobre = new TelaSobre();
		}
		return telaSobre;
	}

	private void actionConfigurar() {
		getTelaConfiguracoes().setVisible(true);
		//getTelaConfigurar().setVisible(true);
	}

	private JFrame getTelaConfiguracoes() {
		if(telaConfiguracoes == null){
			telaConfiguracoes = new TelaConfiguracoes();
		}
		return telaConfiguracoes;
	}

	public PopupMenu getPopup() {
		if(popup == null){
			popup = new PopupMenu("Menu de Opções");
			popup.add(getMenuConfigurar());
			popup.add(getMenuIdentificacao());
			popup.add(getMenuSobre());
			popup.addSeparator();
			popup.add(getMenuItemSair());

		}
		return popup;
	}

	private MenuItem getMenuSobre() {
		if(menuSobre == null){
			menuSobre = new MenuItem("Sobre o GetScan");
			menuSobre.addActionListener(this);
		}
		return menuSobre;
	}

	public void setPopup(PopupMenu popup) {
		this.popup = popup;
	}

	public MenuItem getMenuConfigurar() {
		if(menuConfigurar == null){
			menuConfigurar = new MenuItem("Configurar...");
			menuConfigurar.addActionListener(this);
		}
		return menuConfigurar;
	}

	public void setMenuConfigurar(MenuItem menuConfigurar) {
		this.menuConfigurar = menuConfigurar;
	}

	private TelaIdentificacao getTelaIdentificacao() {
		if(telaIdentificacao == null){
			telaIdentificacao = new TelaIdentificacao();
		}
		return telaIdentificacao;
	}

	private void setTelaIdentificacao(TelaIdentificacao telaIdentificacao) {
		this.telaIdentificacao = telaIdentificacao;
	}

}
