package br.com.getscan.app.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.lang.reflect.Method;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.com.getscan.app.ApplicationContext;
import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.enums.ModoOperacaoEnum;
import br.com.getscan.app.vo.Config;
import br.com.getscan.util.ConfigHelper;
import br.com.getscan.util.ModalFrameUtil;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import br.com.getscan.app.enums.AcaoFinalLinha;

public class TelaConfiguracoes extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtIdentificacao;
	private final ButtonGroup bgModoOperacao = new ButtonGroup();
	private JTextField txtPortaAgente;

	private JButton btnParear;
	private JButton btnSalvar;

	private JComboBox cbAcaoLinha;

	private Config config;

	public JButton getBtnParear() {
		if (btnParear == null) {
			btnParear = new JButton("Parear");
			btnParear.addActionListener(this);
			btnParear.setIcon(new ImageIcon(TelaConfiguracoes.class
					.getResource("/images/search-blue.png")));
			btnParear.setBounds(171, 47, 105, 25);
			btnParear.setVisible(false);
		}
		return btnParear;
	}

	public TelaConfiguracoes() {
		config = ((Config) ApplicationContext.getInstance().getAtribute(
				ConstantesGerais.CONFIGURACOES_GERAIS));

		setAlwaysOnTop(true);
		setTitle("Configurações");
		setResizable(false);
		setSize(338, 483);
		getContentPane().setLayout(null);

		posicionarTela();

		JPanel pnlOpcoes = new JPanel();
		pnlOpcoes.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"),
				"Modo de Opera\u00E7\u00E3o", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		pnlOpcoes.setBounds(8, 107, 318, 81);
		getContentPane().add(pnlOpcoes);
		pnlOpcoes.setLayout(null);

		pnlOpcoes.add(getBtnParear());

		JRadioButton rdStandalone = new JRadioButton("Standalone (Bluetooth)");
		rdStandalone.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (((JRadioButton) e.getSource()).isSelected()) {
					btnParear.setVisible(true);
				} else {
					btnParear.setVisible(false);
				}

			}
		});

		rdStandalone.setBounds(11, 47, 154, 23);
		pnlOpcoes.add(rdStandalone);

		JRadioButton rdRedeLocal = new JRadioButton("Rede local (Wifi)");
		// rdRedeLocal.setSelected(true);
		rdRedeLocal.setBounds(11, 22, 301, 23);
		pnlOpcoes.add(rdRedeLocal);

		bgModoOperacao.add(rdStandalone);
		bgModoOperacao.add(rdRedeLocal);

		JPanel pnlIdentificacao = new JPanel();
		pnlIdentificacao.setBorder(new TitledBorder(null,
				"Identifica\u00E7\u00E3o", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		pnlIdentificacao.setBounds(8, 288, 312, 65);
		getContentPane().add(pnlIdentificacao);
		pnlIdentificacao.setLayout(null);

		txtIdentificacao = new JTextField();
		txtIdentificacao.setHorizontalAlignment(SwingConstants.CENTER);
		txtIdentificacao.setFont(new Font("Tahoma", Font.BOLD, 18));
		txtIdentificacao.setText(((Config) ApplicationContext.getInstance()
				.getAtribute(ConstantesGerais.CONFIGURACOES_GERAIS))
				.getIdentificacao());
		txtIdentificacao.setEditable(false);
		txtIdentificacao.setBounds(10, 21, 298, 28);
		pnlIdentificacao.add(txtIdentificacao);
		txtIdentificacao.setColumns(10);

		JPanel pnlBotoes = new JPanel();
		pnlBotoes.setBackground(Color.WHITE);
		FlowLayout fl_pnlBotoes = (FlowLayout) pnlBotoes.getLayout();
		fl_pnlBotoes.setAlignment(FlowLayout.RIGHT);
		pnlBotoes.setBounds(0, 419, 332, 35);
		getContentPane().add(pnlBotoes);

		pnlBotoes.add(getBtnSalvar());

		JButton btnCancelar = new JButton("Cancelar");
		pnlBotoes.add(btnCancelar);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TelaConfiguracoes.this.dispatchEvent(new WindowEvent(
						TelaConfiguracoes.this, WindowEvent.WINDOW_CLOSING));
			}
		});
		btnCancelar.setIcon(new ImageIcon(TelaConfiguracoes.class
				.getResource("/images/cancel-icon-16.png")));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null,
				"Configura\u00E7\u00F5es Adicionais", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel_1.setBounds(8, 191, 314, 91);
		getContentPane().add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblPortaAgente = new JLabel("Porta do agente");
		GridBagConstraints gbc_lblPortaAgente = new GridBagConstraints();
		gbc_lblPortaAgente.insets = new Insets(0, 0, 5, 5);
		gbc_lblPortaAgente.anchor = GridBagConstraints.EAST;
		gbc_lblPortaAgente.gridx = 0;
		gbc_lblPortaAgente.gridy = 0;
		panel_1.add(lblPortaAgente, gbc_lblPortaAgente);

		txtPortaAgente = new JTextField();
		GridBagConstraints gbc_txtPortaAgente = new GridBagConstraints();
		gbc_txtPortaAgente.insets = new Insets(0, 0, 5, 5);
		gbc_txtPortaAgente.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPortaAgente.gridx = 1;
		gbc_txtPortaAgente.gridy = 0;
		txtPortaAgente.setText(String.valueOf(config.getPortaAgente()));
		panel_1.add(txtPortaAgente, gbc_txtPortaAgente);
		txtPortaAgente.setColumns(10);

		JLabel lblHost = new JLabel("Ação de final de leitura");
		GridBagConstraints gbc_lblHost = new GridBagConstraints();
		gbc_lblHost.anchor = GridBagConstraints.EAST;
		gbc_lblHost.insets = new Insets(0, 0, 0, 5);
		gbc_lblHost.gridx = 0;
		gbc_lblHost.gridy = 1;
		panel_1.add(lblHost, gbc_lblHost);

		cbAcaoLinha = new JComboBox();
		cbAcaoLinha.setModel(new DefaultComboBoxModel(AcaoFinalLinha.values()));
		cbAcaoLinha.setSelectedIndex(0);
		GridBagConstraints gbc_cbAcaoLinha = new GridBagConstraints();
		gbc_cbAcaoLinha.insets = new Insets(0, 0, 0, 5);
		gbc_cbAcaoLinha.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbAcaoLinha.gridx = 1;
		gbc_cbAcaoLinha.gridy = 1;
		panel_1.add(cbAcaoLinha, gbc_cbAcaoLinha);

		JLabel lblDivisor = new JLabel("");
		lblDivisor.setBounds(0, 91, 326, 5);
		lblDivisor.setIcon(new ImageIcon(TelaConfiguracoes.class
				.getResource("/images/division.png")));
		getContentPane().add(lblDivisor);

		JPanel pnlLogo = new JPanel();
		pnlLogo.setBackground(Color.WHITE);
		pnlLogo.setBounds(0, 0, 332, 91);
		getContentPane().add(pnlLogo);

		JLabel lblLogo = new JLabel("");
		pnlLogo.add(lblLogo);
		lblLogo.setIcon(new ImageIcon(TelaConfiguracoes.class
				.getResource("/images/logoGetScan.png")));

		selecionarModoOperacao(config, rdRedeLocal, rdStandalone);
		selecionarAcaoFinalLinha(config, cbAcaoLinha);
	}

	private void selecionarModoOperacao(Config config,
			JRadioButton rdRedeLocal, JRadioButton rdStandalone) {
		rdRedeLocal.setSelected(config.getModoOperacao().equals(
				ModoOperacaoEnum.LOCAL));
		rdStandalone.setSelected(config.getModoOperacao().equals(
				ModoOperacaoEnum.STANDALONE));
	}

	private void selecionarAcaoFinalLinha(Config config, JComboBox cbAcaoLinha) {
		cbAcaoLinha.setSelectedIndex(config.getAcaoFinalLinha().getCodigo()
				.intValue());
	}

	private JButton getBtnSalvar() {
		if (btnSalvar == null) {
			btnSalvar = new JButton("Salvar");
			btnSalvar.setIcon(new ImageIcon(TelaConfiguracoes.class
					.getResource("/images/save-icon-16.png")));
			btnSalvar.addActionListener(this);
		}
		return btnSalvar;
	}

	private void posicionarTela() {
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Dimension screenSize = toolkit.getScreenSize();
		final int x = (screenSize.width - this.getWidth()) - 26;
		final int y = (screenSize.height - this.getHeight()) - 62;
		this.setLocation(x, y);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnParear) {
			acaoBtnParear();
		} else if (e.getSource().equals(getBtnSalvar())) {
			acaoBtnSalvar();
		}

	}

	public String getSelectedRadioButton() {
		for (Enumeration<AbstractButton> buttons = bgModoOperacao.getElements(); buttons
				.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();
			if (button.isSelected()) {
				return button.getText();
			}
		}
		return null;
	}

	private void acaoBtnSalvar() {
		try {
			if (validarDados()) {
				Config novaConfig = (Config) config.clone();

				String opcao = getSelectedRadioButton();
				if (opcao != null) {
					if ("Standalone (Bluetooth)".equals(opcao)) {
						novaConfig.setModoOperacao(ModoOperacaoEnum.STANDALONE);
					} else if ("Rede local (Wifi)".equals(opcao)) {
						novaConfig.setModoOperacao(ModoOperacaoEnum.LOCAL);
					}

				}
				novaConfig.setPortaAgente(Integer.parseInt(txtPortaAgente
						.getText()));
				novaConfig.setAcaoFinalLinha(AcaoFinalLinha
						.getAcaoFinalLinhaByCodigo(Long.parseLong(cbAcaoLinha
								.getSelectedIndex() + "")));
				ConfigHelper.salvarConfig(novaConfig);
				JOptionPane
						.showMessageDialog(
								this,
								"Dados salvos com sucesso!\nO agente será fechado e deverá ser iniciado manualmente!");
				System.exit(0);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane
					.showMessageDialog(
							this,
							"Dados salvos com sucesso!\nO agente será fechado e deverá ser iniciado manualmente!",
							"ERRO", JOptionPane.ERROR_MESSAGE);
		}
		// TODO Auto-generated method stub
	}

	private boolean validarDados() {
		// TODO Auto-generated method stub
		return true;
	}

	private void acaoBtnParear() {
		TelaPareamento telaPareamento = new TelaPareamento();
		ModalFrameUtil.showAsModal(telaPareamento, this);
		RemoteDeviceItem itemSelecionado = ((TelaPareamento) telaPareamento)
				.getItemSelecionado();
		if (itemSelecionado == null) {
			JOptionPane.showMessageDialog(this, "modal frame closed.");
		} else {
			ApplicationContext.getInstance().setAtribute(
					ConstantesGerais.BLUETOOTH_REMOTE_DEVICE,
					itemSelecionado.getDevice());
		}

	}
}
