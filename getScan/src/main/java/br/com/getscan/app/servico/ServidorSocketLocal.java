package br.com.getscan.app.servico;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.util.ICallbackHandler;

public class ServidorSocketLocal extends Thread {

	private static ServerSocket serverSocket = null;

	private ICallbackHandler callback;

	private int porta;

	private int timeout;
	
	private boolean isServidorAtivo = true;

	public ServerSocket getServerSocket() throws IOException {
		if (serverSocket == null) {
			if (getPorta() > 0) {
				serverSocket = new ServerSocket(getPorta());
			} else {
				serverSocket = new ServerSocket(ConstantesGerais.DEFAULT_LOCAL_SERVER_SOCKET_PORT);

			}

			if (getTimeout() > 0) {
				serverSocket.setSoTimeout(getTimeout());
			} else {
				serverSocket.setSoTimeout(ConstantesGerais.DEFAULT_LOCAL_SERVER_SOCKET_TIMEOUT);
			}
		}
		return serverSocket;
	}

	public void run() {
		while (isServidorAtivo) {
			try {
				System.out.println("ServidorLocal waiting for data...");
				Socket server = getServerSocket().accept();
				DataInputStream in = new DataInputStream(
						server.getInputStream());

				String msgContent = in.readUTF();

				if (getCallback() != null) {
					getCallback().processarMensagem(msgContent);
				}

				// DataOutputStream out =
				// new DataOutputStream(server.getOutputStream());
				// out.writeUTF("Thank you for connecting to "
				// + server.getLocalSocketAddress() + "\nGoodbye!");
				server.close();
			} catch (Exception e) {
				throw new RuntimeException("Servico já em execução",e);
			}
		}
	}

	public ICallbackHandler getCallback() {
		return callback;
	}

	public void setCallback(ICallbackHandler callback) {
		this.callback = callback;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public void pararServidor() throws IOException {
		isServidorAtivo = false;
		serverSocket.close();
	}
}
