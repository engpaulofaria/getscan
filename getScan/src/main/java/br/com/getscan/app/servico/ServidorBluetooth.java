package br.com.getscan.app.servico;

import java.io.DataInputStream;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.util.ICallbackHandler;

public class ServidorBluetooth extends Thread {
	
	private static final long UUID_BLUETOOTH = 0x1101; 

	public final static UUID uuid;
	public final static String url;
	private ICallbackHandler callback;

	private LocalDevice local = null;
	private StreamConnectionNotifier server = null;
	private StreamConnection conn = null;

	static {
		uuid = new UUID(UUID_BLUETOOTH);
		url = "btspp://localhost:" + uuid // the service url
				+ ";name=" + ConstantesGerais.NOME_SERVIDOR_BLUETOOTH + ";authenticate=false;encrypt=false;";
	}

	public void run() {
		while (true) {
			try {
				System.out.println("Setting device to be discoverable...");
				local = LocalDevice.getLocalDevice();
				local.setDiscoverable(DiscoveryAgent.GIAC);
				System.out.println("Start advertising service...");
				server = (StreamConnectionNotifier) Connector.open(url);
				System.out.println("Waiting for incoming connection...");
				conn = server.acceptAndOpen();
				System.out.println("Client Connected...");
				DataInputStream din = new DataInputStream(
						conn.openInputStream());
				while (true) {
					String cmd = "";
					char c;
					while (((c = din.readChar()) > 0) && (c != '\n')) {
						cmd = cmd + c;
					}
					if (callback != null) {
						callback.processarMensagem(cmd);
					}
				}

			} catch (Exception e) {
				System.out.println("Exception Occured: " + e.toString());
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

	public ICallbackHandler getCallback() {
		return callback;
	}

	public void setCallback(ICallbackHandler callback) {
		this.callback = callback;
	}

}
