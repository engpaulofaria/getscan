package br.com.getscan.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

	private static ApplicationContext instance = null;
	
	private static Map<String, Object> context;
	
	
	private ApplicationContext(){
		
	}
	
	public static synchronized ApplicationContext getInstance(){
		if(instance == null){
			instance = new ApplicationContext();
		}
		return instance;
	}
	
	
	
	public Object getAtribute(String atributeName){
		return getContext().get(atributeName);
		
	}
	
	public void setAtribute(String atributeName, Object value){
		getContext().put(atributeName, value);
	}
	
	public void clearContext(){
		getContext().clear();
	}
	
	public void removeAtribute(String atributeName){
		getContext().remove(atributeName);
	}

	public Map<String, Object> getContext() {
		if(context == null){
			context = new HashMap<String, Object>();
		}
		return context;
	}

	public void setContext(Map<String, Object> context) {
		this.context = context;
	}
	
	
}
