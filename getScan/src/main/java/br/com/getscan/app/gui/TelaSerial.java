package br.com.getscan.app.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.com.getscan.app.App;
import br.com.getscan.app.ApplicationContext;
import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.vo.Config;
import br.com.getscan.util.ComputerIDUtil;
import br.com.getscan.util.ConfigHelper;
import br.com.getscan.util.ValidadorSerialHelper;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;

public class TelaSerial extends JFrame implements ActionListener {
	private static final String BUILD_DATE = "build.date";

	private static final String APPLICATION_VERSION = "application.version";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Properties versionInfo = null;
	private JTextField textField;
	private JTextField txtSerial;
	private JButton btnFechar;
	
	private String computerId;
	private String serial;
	private JButton btnContinuar;

	public TelaSerial() {
		setResizable(false);
		getContentPane().setBackground(SystemColor.menu);
		setLocationRelativeTo(null);
		setTitle("Serial");
		setSize(432, 305);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 426, 98);
		getContentPane().add(panel);

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(TelaSerial.class
				.getResource("/images/logoGetScan.png")));
		panel.add(lblLogo);

		JLabel lblDivision = new JLabel("");
		lblDivision.setBounds(0, 97, 284, 4);
		lblDivision.setIcon(new ImageIcon(TelaSerial.class
				.getResource("/images/division.png")));
		getContentPane().add(lblDivision);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		FlowLayout flowLayout_1 = (FlowLayout) panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panel_1.setBounds(0, 242, 426, 35);
		getContentPane().add(panel_1);
		
		btnContinuar = new JButton("Continuar");
		panel_1.add(btnContinuar);
		btnContinuar.addActionListener(this);

		btnFechar = new JButton("Fechar");
		panel_1.add(btnFechar);
		btnFechar.addActionListener(this);
		btnFechar.setIcon(new ImageIcon(TelaSerial.class
				.getResource("/images/cancel-icon-16.png")));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 109, 406, 98);
		getContentPane().add(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblIdentificadorDaMquina = new JLabel("Identificador da máquina");
		GridBagConstraints gbc_lblIdentificadorDaMquina = new GridBagConstraints();
		gbc_lblIdentificadorDaMquina.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdentificadorDaMquina.anchor = GridBagConstraints.EAST;
		gbc_lblIdentificadorDaMquina.gridx = 0;
		gbc_lblIdentificadorDaMquina.gridy = 0;
		panel_2.add(lblIdentificadorDaMquina, gbc_lblIdentificadorDaMquina);
		
		setComputerId(ComputerIDUtil.getComputerID());
		
		textField = new JTextField(getComputerId());
		textField.setEditable(false);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel_2.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblSerial = new JLabel("Serial");
		GridBagConstraints gbc_lblSerial = new GridBagConstraints();
		gbc_lblSerial.anchor = GridBagConstraints.EAST;
		gbc_lblSerial.insets = new Insets(0, 0, 5, 5);
		gbc_lblSerial.gridx = 0;
		gbc_lblSerial.gridy = 1;
		panel_2.add(lblSerial, gbc_lblSerial);
		
		txtSerial = new JTextField();
		GridBagConstraints gbc_txtSerial = new GridBagConstraints();
		gbc_txtSerial.insets = new Insets(0, 0, 5, 0);
		gbc_txtSerial.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSerial.gridx = 1;
		gbc_txtSerial.gridy = 1;
		panel_2.add(txtSerial, gbc_txtSerial);
		txtSerial.setColumns(10);
		
		JLabel lblAcesseOUrl = new JLabel("Acesse o link abaixo e informe o Identificador da máquina acima!");
		lblAcesseOUrl.setIcon(new ImageIcon(TelaSerial.class
				.getResource("/images/info-icon-24.png")));
		GridBagConstraints gbc_lblAcesseOUrl = new GridBagConstraints();
		gbc_lblAcesseOUrl.insets = new Insets(0, 0, 5, 0);
		gbc_lblAcesseOUrl.gridwidth = 2;
		gbc_lblAcesseOUrl.gridx = 0;
		gbc_lblAcesseOUrl.gridy = 2;
		panel_2.add(lblAcesseOUrl, gbc_lblAcesseOUrl);
		
		JLabel lblwwwcycocombrgetscanregistrar = new JLabel("<html><a href =\"\">www.cyco.com.br/getScan/registrar</a></html>");
		lblwwwcycocombrgetscanregistrar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblwwwcycocombrgetscanregistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Desktop.getDesktop().browse(
							new URI("http://www.cyco.com.br/getScan/registrar"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_lblwwwcycocombrgetscanregistrar = new GridBagConstraints();
		gbc_lblwwwcycocombrgetscanregistrar.gridwidth = 2;
		gbc_lblwwwcycocombrgetscanregistrar.insets = new Insets(0, 0, 0, 5);
		gbc_lblwwwcycocombrgetscanregistrar.gridx = 0;
		gbc_lblwwwcycocombrgetscanregistrar.gridy = 3;
		panel_2.add(lblwwwcycocombrgetscanregistrar, gbc_lblwwwcycocombrgetscanregistrar);
	}

	

	public synchronized void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(btnFechar)){
			acaoBtnFechar();
		} else if(e.getSource().equals(btnContinuar)){
			acaoBtnContinuar();
		}
	}
	
	private synchronized void acaoBtnContinuar() {
		if(!ValidadorSerialHelper.validarSerial(getComputerId(), txtSerial.getText())){
			JOptionPane.showMessageDialog(
					this,
					"O serial informado não é válido!",
					"ERRO",
					JOptionPane.ERROR_MESSAGE,
					new ImageIcon(TelaSerial.class
							.getResource("/images/close-icon.png")));
		} else {
			try {
				setSerial(txtSerial.getText());
				Config config = ((Config) ApplicationContext
						.getInstance().getAtribute(
								ConstantesGerais.CONFIGURACOES_GERAIS));
				config.setComputerId(getComputerId());
				config.setChaveAcesso(getSerial());
				ConfigHelper.salvarConfig(config);
				JOptionPane.showMessageDialog(
						this,
						"Registro realizado com sucesso!",
						"Sucesso",
						JOptionPane.INFORMATION_MESSAGE,
						new ImageIcon(TelaSerial.class
								.getResource("/images/like-icon.png")));
				
				notify();
				dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(
						this,
						"Erro ao realizar o registro da aplicação!",
						"ERRO",
						JOptionPane.ERROR_MESSAGE,
						new ImageIcon(TelaSerial.class
								.getResource("/images/close-icon.png")));
				System.exit(0);
			}
			
			
		}
		
	}



	private synchronized void acaoBtnFechar() {
		JOptionPane.showMessageDialog(
				this,
				"Não é possivel continuar sem informar o registro...",
				"ERRO",
				JOptionPane.ERROR_MESSAGE,
				new ImageIcon(TelaSerial.class
						.getResource("/images/close-icon.png")));
		System.exit(0);
		
	}



	private String getVersaoAgente(){
		return versionInfo.getProperty(APPLICATION_VERSION);
	}
	
	private String getBuildTimestamp(){
		return versionInfo.getProperty(BUILD_DATE);
		
	}



	public String getComputerId() {
		return computerId;
	}



	public void setComputerId(String computerId) {
		this.computerId = computerId;
	}



	public String getSerial() {
		return serial;
	}



	public void setSerial(String serial) {
		this.serial = serial;
	}
}
