package br.com.getscan.app.enums;

public enum ModoOperacaoEnum {
	LOCAL(1L, "Rede Local (Wifi)"),
	STANDALONE(2L, "Standalone"),
	SELFHOSTED(3L, "Servidor Local (Self-hosted)"),
	CLOUD(4L, "Servidor em núvem");
	
	private Long codigo;
	private String descricao;
	
	private ModoOperacaoEnum(Long codigo, String descricao){
		this.codigo = codigo; 
		this.descricao = descricao;
	}
	
	public Long getCodigo(){
		return codigo;
	}
	
	public String getDescricao(){
		return descricao;
	}
	
	public static ModoOperacaoEnum getModoOperacaoByCodigo(Long codigo){
		for (ModoOperacaoEnum modo : ModoOperacaoEnum.values()) {
			if(modo.getCodigo().equals(codigo)){
				return modo;
			}
		}
		return null;
	}
	
	
}
