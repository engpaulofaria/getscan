package br.com.getscan.app.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import br.com.getscan.app.ApplicationContext;
import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.vo.Config;

public class TelaSobre extends JFrame implements ActionListener {
	private static final String BUILD_DATE = "build.date";

	private static final String APPLICATION_VERSION = "application.version";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Properties versionInfo = null;
	private JTextField txtComputerId;
	private JTextField txtSerial;

	public TelaSobre() {
		carregarVersionInfo();
		
		Config config = ((Config) ApplicationContext
				.getInstance().getAtribute(
						ConstantesGerais.CONFIGURACOES_GERAIS));

		getContentPane().setBackground(SystemColor.menu);
		setLocationRelativeTo(null);
		setTitle("Sobre");
		setSize(424, 392);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 408, 98);
		getContentPane().add(panel);

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(TelaSobre.class
				.getResource("/images/logoGetScan.png")));
		panel.add(lblLogo);

		JLabel lblDivision = new JLabel("");
		lblDivision.setBounds(0, 97, 284, 4);
		lblDivision.setIcon(new ImageIcon(TelaSobre.class
				.getResource("/images/division.png")));
		getContentPane().add(lblDivision);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		FlowLayout flowLayout_1 = (FlowLayout) panel_1.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panel_1.setBounds(0, 319, 408, 35);
		getContentPane().add(panel_1);

		JButton btnFechar = new JButton("Fechar");
		panel_1.add(btnFechar);
		btnFechar.addActionListener(this);
		btnFechar.setIcon(new ImageIcon(TelaSobre.class
				.getResource("/images/cancel-icon-16.png")));

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Sobre o GetScan",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 109, 388, 108);
		getContentPane().add(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblDataDeConstruo = new JLabel("Data de construção:");
		lblDataDeConstruo.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDataDeConstruo = new GridBagConstraints();
		gbc_lblDataDeConstruo.insets = new Insets(0, 0, 5, 5);
		gbc_lblDataDeConstruo.gridx = 0;
		gbc_lblDataDeConstruo.gridy = 0;
		panel_2.add(lblDataDeConstruo, gbc_lblDataDeConstruo);
		
		JLabel lblbDtConstrucao = new JLabel(getBuildTimestamp());
		GridBagConstraints gbc_lblbDtConstrucao = new GridBagConstraints();
		gbc_lblbDtConstrucao.insets = new Insets(0, 0, 5, 0);
		gbc_lblbDtConstrucao.gridx = 2;
		gbc_lblbDtConstrucao.gridy = 0;
		panel_2.add(lblbDtConstrucao, gbc_lblbDtConstrucao);

		JLabel lblVerso = new JLabel("Versão do Agente: ");
		GridBagConstraints gbc_lblVerso = new GridBagConstraints();
		gbc_lblVerso.insets = new Insets(0, 0, 5, 5);
		gbc_lblVerso.gridx = 0;
		gbc_lblVerso.gridy = 1;
		panel_2.add(lblVerso, gbc_lblVerso);
		
		JLabel lblVersaoInfo = new JLabel(getVersaoAgente());
		GridBagConstraints gbc_lblVersaoInfo = new GridBagConstraints();
		gbc_lblVersaoInfo.insets = new Insets(0, 0, 5, 5);
		gbc_lblVersaoInfo.gridx = 1;
		gbc_lblVersaoInfo.gridy = 1;
		panel_2.add(lblVersaoInfo, gbc_lblVersaoInfo);

		JLabel lblCopyrightCyco = new JLabel("Copyright 2014 Cyco Systems. ");
		GridBagConstraints gbc_lblCopyrightCyco = new GridBagConstraints();
		gbc_lblCopyrightCyco.gridwidth = 2;
		gbc_lblCopyrightCyco.insets = new Insets(0, 0, 5, 5);
		gbc_lblCopyrightCyco.gridx = 0;
		gbc_lblCopyrightCyco.gridy = 2;
		panel_2.add(lblCopyrightCyco, gbc_lblCopyrightCyco);

		JLabel lblSite = new JLabel(
				"<html><a href =\"\">www.cyco.com.br/getScan</a></html>");
		GridBagConstraints gbc_lblSite = new GridBagConstraints();
		gbc_lblSite.gridwidth = 2;
		gbc_lblSite.insets = new Insets(0, 0, 0, 5);
		gbc_lblSite.gridx = 0;
		gbc_lblSite.gridy = 3;
		panel_2.add(lblSite, gbc_lblSite);
		lblSite.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Desktop.getDesktop().browse(
							new URI("http://www.cyco.com.br/getScan"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		lblSite.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Sobre o registro",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 222, 388, 86);
		getContentPane().add(panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		JLabel lblIdentificao = new JLabel("Identificação:");
		GridBagConstraints gbc_lblIdentificao = new GridBagConstraints();
		gbc_lblIdentificao.anchor = GridBagConstraints.EAST;
		gbc_lblIdentificao.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdentificao.gridx = 0;
		gbc_lblIdentificao.gridy = 0;
		panel_3.add(lblIdentificao, gbc_lblIdentificao);

		txtComputerId = new JTextField(config.getComputerId());
		txtComputerId.setEditable(false);
		GridBagConstraints gbc_txtComputerId = new GridBagConstraints();
		gbc_txtComputerId.insets = new Insets(0, 0, 5, 0);
		gbc_txtComputerId.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtComputerId.gridx = 1;
		gbc_txtComputerId.gridy = 0;
		panel_3.add(txtComputerId, gbc_txtComputerId);
		txtComputerId.setColumns(10);

		JLabel lblChaveDeAcesso = new JLabel("Chave de Acesso:");
		GridBagConstraints gbc_lblChaveDeAcesso = new GridBagConstraints();
		gbc_lblChaveDeAcesso.anchor = GridBagConstraints.EAST;
		gbc_lblChaveDeAcesso.insets = new Insets(0, 0, 0, 5);
		gbc_lblChaveDeAcesso.gridx = 0;
		gbc_lblChaveDeAcesso.gridy = 1;
		panel_3.add(lblChaveDeAcesso, gbc_lblChaveDeAcesso);

		txtSerial = new JTextField(config.getChaveAcesso());
		txtSerial.setEditable(false);
		GridBagConstraints gbc_txtSerial = new GridBagConstraints();
		gbc_txtSerial.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSerial.gridx = 1;
		gbc_txtSerial.gridy = 1;
		panel_3.add(txtSerial, gbc_txtSerial);
		txtSerial.setColumns(10);
	}

	private void carregarVersionInfo() {
		InputStream in = null;
		try {
			in = new FileInputStream(URLDecoder.decode(TelaSobre.class
					.getClassLoader().getResource("version.properties")
					.getPath(), "UTF-8"));
			versionInfo = new Properties();
			versionInfo.load(in);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void actionPerformed(ActionEvent e) {
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	private String getVersaoAgente() {
		return versionInfo.getProperty(APPLICATION_VERSION);
	}

	private String getBuildTimestamp() {
		return versionInfo.getProperty(BUILD_DATE);

	}
}
