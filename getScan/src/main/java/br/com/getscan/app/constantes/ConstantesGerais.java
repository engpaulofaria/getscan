package br.com.getscan.app.constantes;

public interface ConstantesGerais {
	public static final String URL_SERVIDOR_CLOUD = "playanywhere.com.br";
	public static final int DEFAULT_LOCAL_SERVER_SOCKET_TIMEOUT = 0; //Aguarda uma conexão indefinidamente
	public static final int DEFAULT_LOCAL_SERVER_SOCKET_PORT = 9999;
	public static final String NOME_SERVIDOR_BLUETOOTH = "GetScanAgent";
	public static final String CONFIGURACOES_GERAIS = "CONFIG_GERAIS";
	public static final String BLUETOOTH_REMOTE_DEVICE = "BLUETOOTH_REMOTE_DEVICE";
}
