package br.com.getscan.app.vo;

import java.io.Serializable;

import br.com.getscan.app.enums.AcaoFinalLinha;
import br.com.getscan.app.enums.ModoOperacaoEnum;

public class Config implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ModoOperacaoEnum modoOperacao;
	private int portaAgente;
	private String caminhoServidor;
	private int portaServidor;
	private String identificacao;
	private AcaoFinalLinha acaoFinalLinha;
	private String computerId;
	private String chaveAcesso;
	
	

	public ModoOperacaoEnum getModoOperacao() {
		return modoOperacao;
	}

	public void setModoOperacao(ModoOperacaoEnum modoOperacao) {
		this.modoOperacao = modoOperacao;
	}

	public String getCaminhoServidor() {
		return caminhoServidor;
	}

	public void setCaminhoServidor(String caminhoServidor) {
		this.caminhoServidor = caminhoServidor;
	}

	public int getPortaServidor() {
		return portaServidor;
	}

	public void setPortaServidor(int portaServidor) {
		this.portaServidor = portaServidor;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public int getPortaAgente() {
		return portaAgente;
	}

	public void setPortaAgente(int portaAgente) {
		this.portaAgente = portaAgente;
	}

	public AcaoFinalLinha getAcaoFinalLinha() {
		return acaoFinalLinha;
	}

	public void setAcaoFinalLinha(AcaoFinalLinha acaoFinalLinha) {
		this.acaoFinalLinha = acaoFinalLinha;
	}

	public String getComputerId() {
		return computerId;
	}

	public void setComputerId(String computerId) {
		this.computerId = computerId;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
}
