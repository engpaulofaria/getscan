/*
 * BluetoothBrowser.java
 *
 * Copyright (C) 2007 Asterbox.com
 * http://www.asterbox.com
 *
 */

package br.com.getscan.app.servico;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

/**
 *
 * @author Tommi Laukkanen (tommi at asterbox dot com)
 */
public class BluetoothBrowser implements DiscoveryListener {

    private LocalDevice localDevice;
    private DiscoveryAgent agent;
    
    public List<RemoteDevice> devices;
    
    private IInquiryCompleHandler inquiryCompleHandler;
    
    /** Creates a new instance of BluetoothBrowser */
    public BluetoothBrowser(IInquiryCompleHandler inquiryCompleHandler) throws BluetoothStateException {
    	this.inquiryCompleHandler = inquiryCompleHandler;
        localDevice = LocalDevice.getLocalDevice();
        agent = localDevice.getDiscoveryAgent();
    }
    
    public void inquiry() throws BluetoothStateException {
        agent.startInquiry(DiscoveryAgent.GIAC, this);
    }

    public void deviceDiscovered(RemoteDevice device, DeviceClass devClass) {
        try {
        	getDevices().add(device);
            System.out.println("Device discovered: " + device.getFriendlyName(false));
        } catch (IOException ex) {
            Logger.getLogger("global").log(Level.SEVERE, null, ex);
        }
    }

    public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
    	System.out.println("Service discovered");
    }

    public void serviceSearchCompleted(int transID, int responseCode) {
    	System.out.println("Service search completed");
    }

    public void inquiryCompleted(int discType) {
    	System.out.println("Inquiry completed");
    	if(inquiryCompleHandler != null){
    		inquiryCompleHandler.inquiryCompleted();
    	}
    }

	public List<RemoteDevice> getDevices() {
		if(devices == null){
			devices = new ArrayList<RemoteDevice>();
		}
		return devices;
	}

	public void setDevices(List<RemoteDevice> devices) {
		this.devices = devices;
	}

}
