package br.com.getscan.app.teste;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ClienteSocket {
	public static void main(String[] args) {
		String serverName = "localhost";
		int port = 9999;
		try {
			Socket client = new Socket(serverName, port);
			System.out.println("Just connected to "
					+ client.getRemoteSocketAddress());
			OutputStream outToServer = client.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);

			out.writeUTF("Hello world");
			client.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
