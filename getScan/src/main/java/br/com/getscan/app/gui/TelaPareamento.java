package br.com.getscan.app.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.bluetooth.RemoteDevice;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import br.com.getscan.app.App;
import br.com.getscan.app.servico.BluetoothBrowser;
import br.com.getscan.app.servico.IInquiryCompleHandler;

public class TelaPareamento extends JFrame implements IInquiryCompleHandler, ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	final JList list;
	
	private JButton btnPesquisar;
	
	private JButton btnSelecionar;
	private JButton btnCancelar;
	
	private JFrame telaPai;
	
	private RemoteDeviceItem itemSelecionado;
	
	
	public JFrame getTelaPai() {
		return telaPai;
	}

	public void setTelaPai(JFrame telaPai) {
		this.telaPai = telaPai;
	}

	public void searchDevices() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run()  {
            	getBtnPesquisar().setEnabled(false);
                Thread.yield();
                try {
                    BluetoothBrowser browser = new BluetoothBrowser(TelaPareamento.this);
                    browser.inquiry();
                    Thread.yield();
                    Thread.sleep(12000);
                    if(browser.getDevices().size()>0){
                    	Vector<RemoteDeviceItem> vector = new Vector<RemoteDeviceItem>();
                    	for (RemoteDevice device : browser.devices){
                    		vector.add(new RemoteDeviceItem(device));
                    	}
                    	synchronized (list) {
                    		list.setListData(vector);
						}
                    }
                } catch(Exception ex) {
                }       
                Thread.yield();
                getBtnPesquisar().setEnabled(true);
            }
        }); 
     
    }
	
	public TelaPareamento() {
		setAlwaysOnTop(true);
		setTitle("Seleção de dispositivos");
		setResizable(false);
		setSize(448, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 

		JPanel pnlPrincipal = new JPanel();
		getContentPane().add(pnlPrincipal, BorderLayout.CENTER);
		pnlPrincipal.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Dispositivos Encontrados",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlPrincipal.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		list = new JList();
		panel.add(list, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.SOUTH);

		JLabel lblNewLabel = new JLabel("Tornar o Bluetooth do seu smartphone detectável!");
		lblNewLabel.setIcon(new ImageIcon(TelaPareamento.class
				.getResource("/images/alert-icon.png")));
		panel_1.add(lblNewLabel);

		JPanel pnlBotoes = new JPanel();
		getContentPane().add(pnlBotoes, BorderLayout.SOUTH);

		
		pnlBotoes.add(getBtnSelecionar());
		pnlBotoes.add(getBtnPesquisar());
		pnlBotoes.add(getBtnCancelar());

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				searchDevices();
			}
		});
	}

	private JButton getBtnCancelar() {
		if(btnCancelar == null){
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(this);
			btnCancelar
			.setIcon(new ImageIcon(
					TelaConfiguracoes.class.getResource("/images/cancel-icon-16.png")));
		}
		return btnCancelar;
	}

	private JButton getBtnPesquisar() {
		if(btnPesquisar == null){
			btnPesquisar = new JButton("Pesquisar novamente...");
			btnPesquisar.addActionListener(this);
		}
		return btnPesquisar;
	}

	private JButton getBtnSelecionar() {
		if(btnSelecionar == null){
			btnSelecionar = new JButton("Selecionar");
			btnSelecionar.setIcon(new ImageIcon(
					TelaConfiguracoes.class.getResource("/images/select-icon-16.png")));
			btnSelecionar.addActionListener(this);
		}
		return btnSelecionar;
	}

	public RemoteDeviceItem getItemSelecionado() {
		return itemSelecionado;
	}
	
	public void setItemSelecionado(RemoteDeviceItem item){
		itemSelecionado = item;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == getBtnPesquisar()){
			acaoBtnPesquisar();
		} else if(e.getSource() == btnSelecionar){
			acaoBtnSelecionar();
		} else if(e.getSource() == getBtnCancelar()){
			acaoBtnCancelar();
		}
		
	}

	private synchronized void acaoBtnCancelar() {
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));	
		notify();
	}

	private synchronized void acaoBtnSelecionar() {
		itemSelecionado = (RemoteDeviceItem) list.getModel().getElementAt(list.getSelectedIndex());
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
		notify();
		
	}

	private void acaoBtnPesquisar() {
		searchDevices();		
	}

	public void inquiryCompleted() {
		JOptionPane.showMessageDialog(this, "Busca por dispositivos Bluetooth concluída.\nCaso seu dispositivo não esteja listado, uma nova pesquisa poderá ser realizada!");
		
	}

}
