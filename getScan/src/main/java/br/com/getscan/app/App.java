package br.com.getscan.app;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import br.com.getscan.app.constantes.ConstantesGerais;
import br.com.getscan.app.enums.ModoOperacaoEnum;
import br.com.getscan.app.gui.RemoteDeviceItem;
import br.com.getscan.app.gui.ScanTray;
import br.com.getscan.app.gui.TelaPareamento;
import br.com.getscan.app.gui.TelaSerial;
import br.com.getscan.app.servico.ServidorBluetooth;
import br.com.getscan.app.servico.ServidorSocketLocal;
import br.com.getscan.app.vo.Config;
import br.com.getscan.util.ComputerIDUtil;
import br.com.getscan.util.ConfigHelper;
import br.com.getscan.util.ICallbackHandler;
import br.com.getscan.util.KeyboadCallbackHandler;
import br.com.getscan.util.ValidadorSerialHelper;

/**
 * Hello world!
 * 
 */
public class App implements Runnable {

	final static Logger LOGGER = Logger.getLogger(App.class);

	private Config config;

	public static void main(String[] args) {

		// inicializarApp();
		new App().run();

	}

	private void inicializarApp() {
		try {

			config = carregarConfiguracoes();

			validarSerial(config);

			ICallbackHandler callback = new KeyboadCallbackHandler();

			// Inicializa o servidor local
			inicializarServidorLocal(callback, config);

			// Inicializa o servidor bluetooth
			inicializarServidorBluetooth(callback, config);

			aplicarLookAndFeel();

			ScanTray.getInstance().getTray();

		} catch (RuntimeException e) {
			LOGGER.error(e.getMessage());
			JOptionPane.showMessageDialog(
					null,
					e.getMessage(),
					"ERRO",
					JOptionPane.ERROR_MESSAGE,
					new ImageIcon(TelaSerial.class
							.getResource("/images/close-icon.png")));
			System.exit(0);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			JOptionPane.showMessageDialog(
					null,
					e.getMessage(),
					"ERRO",
					JOptionPane.ERROR_MESSAGE,
					new ImageIcon(TelaSerial.class
							.getResource("/images/close-icon.png")));
			System.exit(0);
		}
	}

	private void validarSerial(Config config2) {
		if (config2.getComputerId() != null && config2.getChaveAcesso() != null
				&& !"".equals(config2.getComputerId())
				&& !"".equals(config2.getChaveAcesso())) {
			if (!ComputerIDUtil.getComputerID().equals(config2.getComputerId())) {
				JOptionPane
						.showMessageDialog(
								null,
								"O Identificador da maquina não pertençe a este computador.",
								"ERRO",
								JOptionPane.ERROR_MESSAGE,
								new ImageIcon(TelaSerial.class
										.getResource("/images/close-icon.png")));
				config2.setComputerId("");
				config2.setChaveAcesso("");
				try {
					ConfigHelper.salvarConfig(config2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.exit(0);
			} else {
				if (!ValidadorSerialHelper.validarSerial(
						config2.getComputerId(), config2.getChaveAcesso())) {
					JOptionPane.showMessageDialog(
							null,
							"O serial registrado é inválido.",
							"ERRO",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(App.class
									.getResource("/images/close-icon.png")));
					config2.setComputerId("");
					config2.setChaveAcesso("");
					try {
						ConfigHelper.salvarConfig(config2);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.exit(0);
				}
			}
		} else {

			TelaSerial telaSerial = new TelaSerial();
			telaSerial.setVisible(true);
			synchronized (telaSerial) {
				try {
					telaSerial.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	private Config carregarConfiguracoes() throws Exception {
		Config config = ConfigHelper.carregarConfig();
		ApplicationContext.getInstance().setAtribute(
				ConstantesGerais.CONFIGURACOES_GERAIS, config);
		return config;
	}

	private void inicializarServidorBluetooth(ICallbackHandler callback,
			Config config2) {
		if (config2.getModoOperacao().equals(ModoOperacaoEnum.STANDALONE)) {
			TelaPareamento telaPareamento = new TelaPareamento();
			telaPareamento.setVisible(true);
			synchronized (telaPareamento) {
				try {
					telaPareamento.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// ModalFrameUtil.showAsModal(telaPareamento, this);
				RemoteDeviceItem itemSelecionado = ((TelaPareamento) telaPareamento)
						.getItemSelecionado();
				if (itemSelecionado == null) {
					JOptionPane.showMessageDialog(
							null,
							"Nenhum dispositivo foi pareado!",
							"ERRO",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(App.class
									.getResource("/images/close-icon.png")));
					System.exit(0);
				} else {
					ServidorBluetooth servidorBluetooth = new ServidorBluetooth();
					servidorBluetooth.setCallback(callback);
					servidorBluetooth.start();
				}

			}
		}
	}

	private void inicializarServidorLocal(ICallbackHandler callback,
			Config config2) throws Exception {
		if (config2.getModoOperacao().equals(ModoOperacaoEnum.LOCAL)) {
			Socket client = null;
			try {
				client = new Socket("localhost", config2.getPortaAgente());
				throw new Exception("O Agente já se encontra em execução!");
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (client != null) {
					try {
						client.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			ServidorSocketLocal servidorLocal = new ServidorSocketLocal();
			servidorLocal.setCallback(callback);
			servidorLocal.setPorta(config2.getPortaAgente());
			servidorLocal.start();
		}
	}

	private void aplicarLookAndFeel() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				System.out.println(info.getName());
				if ("Windows".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception ex) {
			System.out.println("Erro ao aplicar o lookandfeel");
		}
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	@Override
	public void run() {
		inicializarApp();
	}

}
