package br.com.getscan.app.servico;

import java.util.EventListener;

public interface IInquiryCompleHandler extends EventListener {
	 public void inquiryCompleted();
}
