(function() {
    var app = angular.module('commons', [])
        .factory('Utils',['$window', function($window) {
            return {

                //Redireciona para uma url
                /**
                 * @param [String] url
                 */
                redireciona: function redireciona(url) {
                    $window.location.assign(url);
                },
                
                //Realiza o history back
                back: function back() {
                    $window.history.back();
                }
            };
        }])
        .factory('StringUtils',['$window', function($window) {
            return {

                //Verifica se uma String é vazia ou nula
                /**
                 * @param [String] value
                 */
                isBlank: function isBlank(value) {
                    if (!value || value === null || value.trim() === '') {
                        return true;
                    }
                    return false;
                },
                //Verifica se uma String não é vazia ou nula
                /**
                 * @param [String] value
                 */
                isNotBlank: function isNotBlank(value) {
                    if (value && value !== null && value.trim() !== '') {
                        return true;
                    }
                    return false;
                },
                //Verifica se uma String é igual a outra String
                /**
                 * @param [String] value1
                 * @param [String] value2
                 */
                isEqual: function isEqual(value1, value2) {
                    if (value1 && value2 && value1 === value2) {
                        return true;
                    }
                    return false;
                }
            };
        }]);
        
})();