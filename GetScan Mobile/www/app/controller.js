(function() {
    var app = angular.module('controller', []);
    app.controller('homeController', ['$window', '$scope', 'Utils', 'StringUtils', 'globalSettingsService', function($window, $scope, Utils, StringUtils, globalSettingsService) {
        globalSettingsService.restoreState();
        
        this.canChangeRoom = function canChangeRoom() {
            var result = false;
            
            var globalSettings = globalSettingsService.model;
            
            if (globalSettings !== null && StringUtils.isNotBlank(globalSettings.cpf) && StringUtils.isNotBlank(globalSettings.url)) {
                result = true;
            }
            
            return result;
        };
        
        this.canRegistry = function canRegistry() {
            var result = false;
            
            var globalSettings = globalSettingsService.model;
            
            if (this.canChangeRoom() && StringUtils.isNotBlank(globalSettings.evento) && StringUtils.isNotBlank(globalSettings.sala)) {
                result = true;
            }
            return result;
        };
        
        this.goToSettings = function goToSettings() {
            Utils.redireciona('app/settings/settings.html');
        };
        
        this.goToHelp = function goToHelp() {
            Utils.redireciona('app/help/help.html');
        };
        
        this.goToChangeRoom = function goToChangeRoom() {
            Utils.redireciona('app/room/changeRoom.html');
        };
        
        this.goToRegistry = function goToRegistry() {
            Utils.redireciona('app/register/registry.html');
        };
        
        this.close = function close() {
            navigator.app.exitApp();
        };
    }]);
    app.controller('registryController', ['$scope', '$route', '$window', '$location', 'Utils', 'registryService', function($scope, $route, $window, $location, Utils, registryService) {
        $scope.dadosTela = {};
        $scope.dadosQrCode = {};
        
        $scope.mensagens = {
            sucesso: [],
            informacao: [],
            alerta: [],
            erro: []
        };
        
        this.readQrCode = function() {
            document.addEventListener("intel.xdk.device.barcode.scan",function(evt){
                $scope.mensagens = {
                    sucesso: [],
                    informacao: [],
                    alerta: [],
                    erro: []
                };

                intel.xdk.notification.beep(1);
                if (evt.success === true) {
                    //successful scan

                    $scope.dadosQrCode = evt.codedata;
                    //$scope.dadosQrCode = {id: '3', evento: {id: '3', salas: ['6', '7']}};

                    console.log($scope.dadosQrCode);
                    
                    if (!$scope.dadosQrCode || $scope.dadosQrCode === null) {
                        $scope.mensagens.erro.push('Falha ao obter dados pós leitura do QR-Code');
                    } else {
                        $scope.mensagens.sucesso.push('Leitura do QR-Code realizada.');
                        
                        var object = registryService.registrar($scope.dadosQrCode);

                        if (object) {
                            if (object.erro) {
                                $scope.$apply(function() {
                                    $scope.mensagens.erro = angular.copy(object.erro);
                                });
                            } else {
                                var responsePromise = object;

                                responsePromise.then(
                                    function(data) {
                                        callbackRegistry(data);
                                    },
                                    function(reason) {
                                        console.log(reason);
                                        $scope.mensagens.erro.push(reason);
                                    }
                                );
                                
                                var callbackRegistry = function callbackRegistry(data) {
                                    var msg = angular.fromJson(data).msg;
                                    var inscricao;

                                    if (msg) {
                                        $scope.mensagens.alerta.push(msg);
                                    } else {
                                        $scope.mensagens.sucesso.push(data);
                                    }
                                };
                            }
                        }
                    }
                } else {
                    //failed scan
                    console.log("failed scan");
                    $scope.mensagens.erro.push('Falha ao ler o QR-Code.');
                }
            },false);

            intel.xdk.device.scanBarcode();
        };
        
        this.goToHome = function() {
            Utils.back();
        };
        
        
    }]);
    app.controller('settingsController', ['$scope', 'Utils', 'settingsService', function($scope, Utils, settingsService) {
        var settings = settingsService.recuperarDadosConfiguracao();
        this.onlyNumber = /^\d+$/;
        this.dadosTela = {};
        
        this.dadosTela.showUrl = false;
        this.dadosTela.showCpf = false;
        
        $scope.checkeboxSelect = function(){
            console.log('1 ' + $scope.checked);
            console.log('2 ' + $scope.checked1);
            if($scope.checked){
                $scope.checked1 = false;
                $scope.checked1 = false;
            }
            
            if($scope.checked1){
                $scope.checked = false;
                $scope.checked = false;
            }
        };
        
        $scope.mensagens = {
            sucesso: [],
            informacao: [],
            alerta: [],
            erro: []
        };
        
        if (settings) {
            this.dadosTela.cpf = settings.cpf;
            this.dadosTela.url = settings.url;
        }
        
        this.goToHome = function() {
            //Utils.redireciona('../../index.html');
            Utils.back();
        };
        
        this.salvar = function() {
			$scope.mensagens = settingsService.gravarDadosConfiguracao(this.dadosTela);
        };
    }]);
    app.controller('changeRoomController', ['$scope', '$route', 'Utils', 'changeRoomService', function($scope, $route, Utils, changeRoomService) {
        $scope.mensagens = {
            sucesso: [],
            informacao: [],
            alerta: [],
            erro: []
        };
        
		$scope.dadosTela = {};
        
		var dados = changeRoomService.recuperarDadosConfiguracao();
        
		var responsePromise = changeRoomService.recuperarEventosCadastrados();
					
		responsePromise.then(
			function(data) {
				callbackRecuperarEventosCadastrados(data);
			},
            function(reason) {
				console.log(reason);
                $scope.mensagens.erro.push(reason);
                $route.reload();
            }
		);
		
		var callbackRecuperarEventosCadastrados = function callbackRecuperarEventosCadastrados(data) {
			$scope.dadosTela = {
				seHabilitado: false,
				eventos: [],
				eventoSelecionado: {},
				salaSelecionada: {}
			};
			
			var msg = angular.fromJson(data).msg;
			
			if (msg) {
				$scope.mensagens.alerta.push(msg);
			} else {
				eventos = data;
				
				if (eventos) {
					$scope.dadosTela.seHabilitado = true;
					$scope.dadosTela.eventos = angular.fromJson(eventos);
					
					if (dados && dados.idEvento) {
						for (var i = 0; i < $scope.dadosTela.eventos.length; i++) {
							var evento = $scope.dadosTela.eventos[i];
							
							if (evento && evento.idEvento === dados.idEvento) {
								$scope.dadosTela.eventoSelecionado = evento;
								
								if (dados.idSala) {
									for (var j = 0; j < evento.salas.length; j++) {
										var sala = evento.salas[j];
										
										if (sala && sala.idSala === dados.idSala) {
											$scope.dadosTela.salaSelecionada = sala;
											break;
										}
									}
								}
								break;
							}
						}
					}
				}
			}
		};
		
		this.canChooseRoom = function canChooseRoom(evento) {
            var result = false;
            
            if (evento) {
                result = true;
            }
            
            return result;
        };
        this.limparSelecaoSala = function() {
            if ($scope.dadosTela && $scope.dadosTela.salaSelecionada) {
                $scope.dadosTela.salaSelecionada = null;
            }
        };
        this.salvar = function() {
            var dados = {};
            dados.idEvento = $scope.dadosTela.eventoSelecionado.idEvento;
            dados.idSala = $scope.dadosTela.salaSelecionada.idSala;
            $scope.mensagens = changeRoomService.gravarDadosConfiguracao(dados);
        };
        this.goToHome = function() {
            Utils.back();
        };
        
    }]);
    
})();