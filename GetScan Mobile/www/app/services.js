(function() {
    var app = angular.module('services', [])
		.factory('connectService',['$rootScope', 'localStorageService', 'jsonService', function($rootScope, localStorageService, jsonService) {
			var service = {
				connect: function connect(service, action, params) {
                    var globalSettings = angular.fromJson(localStorageService.getItem('globalSettings'));
                    var responsePromise = null;
					
                    if (globalSettings) {

                        var urlServico = globalSettings.url;

                        if (urlServico) {
                            if (urlServico.indexOf('/', urlServico.length - 1) === -1) {
                                urlServico = urlServico.concat('/');
                            }
                            
                            var comando = 'servico/';
                            comando = comando.concat(service);
                            comando = comando.concat('/');
                            comando = comando.concat(action);
                            comando = comando.concat('/');
                            
                            if (params) {
                                for (var i = 0; i < params.length; i++) {
                                    comando = comando.concat(params[i]);
                                    comando = comando.concat('/');
                                }
                            }
                            comando = comando.concat('?callback=JSON_CALLBACK');
                            
                            console.log('URL Serviço: ', urlServico);
                            console.log('Comando: ', comando);
                            console.log('URL Completa: ', urlServico.concat(comando));
                            //alert(urlServico);
                            //alert(comando);

                            responsePromise = jsonService.getDataPromise(urlServico.concat(comando));

                        }
					}
                    
					return responsePromise;
				}
			};
			return service;
		}])
        .factory('jsonService',['$rootScope', '$http', '$q', function($rootScope, $http, $q) {
			var service = {
				getDataPromise: function getDataPromise(url) {
					var deferred = $q.defer();
							
					$http.get(url)
						.then(
							function(resp) {
								deferred.resolve(
									angular.toJson(resp.data));
							},
							function(err) {
                                //alert(angular.toJson(err));
								deferred.reject('Erro de comunicação com o servidor.');
								console.log('Erro de comunicação com o servidor: ', angular.toJson(err));
							});

					return deferred.promise;
				}
			};
			return service;
		}])
        .factory('localStorageService', ['$window', function($window) {
            var service = {
                /**
                 * @param {String} key
                 * @param {String} value
                 */
                setItem: function setItem(key, value)  {
                    $window.localStorage.setItem(key, value);
                },
                /**
                 * @param {String} key
                 */
                getItem: function getItem(key) {
                    return $window.localStorage.getItem(key);
                }
            };
            
            return service;
        }])
        .factory('globalSettingsService',['$rootScope', 'settingsService', 'changeRoomService', function($rootScope, settingsService, changeRoomService) {
            var service = {
                model: {
                    cpf: '',
                    evento: '',
                    sala: '',
                    url: '',
                    radioValueLocal: ''
                },
                mensagens: {
                    sucesso: [],
                    informacao: [],
                    alerta: [],
                    erro:[]
                },
                restoreState: function restoreState() {
                    var restoredGlobalSettingsModel = settingsService.recuperarDadosConfiguracao();
                    var restoredRoomSettingsModel = changeRoomService.recuperarDadosConfiguracao();
					
                    if (restoredGlobalSettingsModel) {
                        service.model.cpf = restoredGlobalSettingsModel.cpf;
                        service.model.url = restoredGlobalSettingsModel.url;
                        service.model.radioValueLocal =restoredGlobalSettingsModel.radioValueLocal;
                    }
                    
                    if (restoredRoomSettingsModel) {
                        service.model.evento = restoredRoomSettingsModel.idEvento;
                        service.model.sala = restoredRoomSettingsModel.idSala;
                    }
                }
            };
            return service;
        }])
        .factory('registryService',['$rootScope', 'localStorageService', 'connectService', function($rootScope, localStorageService, connectService) {
            var service = {
                model: {
                    inscricao: ''
                },
                
                mensagens: {
                    sucesso: [],
                    informacao: [],
                    alerta: [],
                    erro:[]
                },
                
                registrar: function registrar(dadosQrCode) {
                    
                    service.mensagens = {
                        sucesso: [],
                        informacao: [],
                        alerta: [],
                        erro:[]
                    };
                    
                    var globalSettings = angular.fromJson(localStorageService.getItem('globalSettings'));
                    var salaSettings = angular.fromJson(localStorageService.getItem('salaSettings'));
					var inscricao = null;
                    var responsePromise = null;
                    var isValido = true;
                    
                    console.log('Registry Service: ', dadosQrCode)

                    if (!dadosQrCode) {
                        service.mensagens.erro.push('Não foi possível recuperar dados do QR-Code.');
                        isValido = false;
                    } else {
                        inscricao = angular.fromJson(dadosQrCode);
                        
                        var cpf = globalSettings.cpf;

                        if (inscricao && inscricao.id) {
                            if (inscricao.evento && inscricao.evento.id) {
                                if (inscricao.evento.id === salaSettings.idEvento) {
                                    var salas = inscricao.evento.salas;
                                    if (salas && salas.length > 0) {
                                        var sala = null;

                                        for (var i = 0; i < salas.length; i++) {
                                            if (salas[i] === salaSettings.idSala) {
                                                sala = salas[i];
                                                break;
                                            }
                                        }

                                        if (sala && sala !== null) {
                                            responsePromise = connectService.connect('presenca', 'registrar', [cpf, sala, inscricao.id]);
                                        } else {
                                            service.mensagens.erro.push('A Sala do Fiscal não está na relação de salas inscritas para o Participante.');
                                            isValido = false;
                                        }
                                    } else {
                                        service.mensagens.erro.push('Não há Salas cadastradas para o Evento do Participante.');
                                        isValido = false;
                                    }
                                } else {
                                    service.mensagens.erro.push('Inscrição não pertence ao Evento cadastrado para o Fiscal.');
                                    isValido = false;
                                }
                            } else {
                                service.mensagens.erro.push('Não há Evento cadastrado para o Participante.');
                                isValido = false;
                            }
                        } else {
                            service.mensagens.erro.push('Dados de inscrição não válidos.');
                            isValido = false;
                        }
                    }
					
                    if (!responsePromise || responsePromise === null) {
                        service.mensagens.erro.push('Registro de presença não realizado.');
                        return service.mensagens;
                    }
                    
                    return responsePromise;
                }
            };
            return service;
        }])
        .factory('settingsService',['$rootScope', 'localStorageService', function($rootScope, localStorageService) {
            var service = {
                model: {
                    url: '',
                    cpf: '',
                    radioValueLocal: ''
                },
                
                mensagens: {
                    sucesso: [],
                    informacao: [],
                    alerta: [],
                    erro:[]
                },
                
                recuperarDadosConfiguracao: function recuperarDadosConfiguracao() {
                    var globalSettings = angular.fromJson(localStorageService.getItem('globalSettings'));
                    
                    if (globalSettings) {
                        service.model.url = globalSettings.url;
                        service.model.cpf = globalSettings.cpf;
                        service.model.radioValueLocal = globalSettings.radioValueLocal;
                    }
                    
                    if (!service.model.url) {
                        service.model.url = '';
                    }
                    
                    return service.model;
                },
                
                gravarDadosConfiguracao: function gravarDadosConfiguracao(settings) {
                    function validarDados() {
                        var isValido = true;
                        if (!settings) {
                            service.mensagens.erro.push('Necessário informar URL e CPF para salvar.');
                            isValido = false;
                        } else {
                            if (!settings.url || !settings.url.trim()) {
                                service.mensagens.erro.push('O campo URL é obrigatório.');
                                isValido = false;
                            }
                            if (!settings.cpf || !settings.cpf.trim()) {
                                service.mensagens.erro.push('O campo CPF é obrigatório.');
                                isValido = false;
                            }
                        }
                        
                        return isValido;
                    }
                    
                    service.mensagens = {
                        sucesso: [],
                        informacao: [],
                        alerta: [],
                        erro:[]
                    };
                    
                    if (validarDados()) {
						var globalSettings = angular.fromJson(localStorageService.getItem('globalSettings'));
						
						if (!globalSettings) {
							globalSettings = {};
						}
						
                        if (globalSettings.cpf && globalSettings.cpf !== settings.cpf) {
                            var salaSettings = angular.fromJson(localStorageService.getItem('salaSettings'));
                            salaSettings.idEvento = null;
                            salaSettings.idSala = null;
                            localStorageService.setItem('salaSettings', angular.toJson(salaSettings));
                        }
                        
                        globalSettings.cpf = settings.cpf;
                        globalSettings.url = settings.url;
                        
                        localStorageService.setItem('globalSettings', angular.toJson(globalSettings));
                        
						service.mensagens.sucesso.push('Dados salvo com sucesso.');
                    }
                    
                    return service.mensagens;
                }
            };
            return service;
        }])
        .factory('changeRoomService',['$rootScope', 'localStorageService', 'connectService', function($rootScope, localStorageService, connectService) {
            var service = {
                model: {},
                
                mensagens: {
                    sucesso: [],
                    informacao: [],
                    alerta: [],
                    erro:[]
                },
				
				recuperarEventosCadastrados: function recuperarEventosCadastrados() {
                    var globalSettings = angular.fromJson(localStorageService.getItem('globalSettings'));
                    var eventos = connectService.connect('evento', 'listar', [globalSettings.cpf]);
                    
                    if (eventos === null) {
                        service.mensagens.erro.push('Não há eventos cadastrados');
                    }
                    
                    return eventos;
				},
                
                recuperarDadosConfiguracao: function recuperarDadosConfiguracao() {
					
					var salaSettings = angular.fromJson(localStorageService.getItem('salaSettings'));
				
					if (salaSettings) {
						service.model.idEvento = salaSettings.idEvento;
						service.model.idSala = salaSettings.idSala;
					}
                    
                    return service.model;
                },
                
                gravarDadosConfiguracao: function gravarDadosConfiguracao(dados) {
                    function validarDados() {
                        var isValido = true;
                        
                        if (!dados) {
                            service.mensagens.erro.push('Necessário informar Evento e Sala para salvar.');
                            isValido = false;
                        } else {
                            if (!dados.idEvento || !dados.idEvento.trim()) {
                                service.mensagens.erro.push('O campo Evento é obrigatório.');
                                isValido = false;
                            }
                            if (!dados.idSala || !dados.idSala.trim()) {
                                service.mensagens.erro.push('O campo Sala é obrigatório.');
                                isValido = false;
                            }
                        }
                        
                        return isValido;
                    }
                    
                    service.mensagens = {
                        sucesso: [],
                        informacao: [],
                        alerta: [],
                        erro:[]
                    };
                    
                    if (validarDados()) {
                        var salaSettings = angular.fromJson(localStorageService.getItem('salaSettings'));
                        
                        if (!salaSettings) {
                            salaSettings = {};
                        }
                        
                        salaSettings.idEvento = dados.idEvento;
                        salaSettings.idSala = dados.idSala;
                        
                        localStorageService.setItem('salaSettings', angular.toJson(salaSettings));
                        
                        service.mensagens.sucesso.push('Dados salvo com sucesso.');
                    }
                    
                    return service.mensagens;
                }
            };
            return service;
        }]);

})();