(function() {
    var app = angular.module('directives', [])
        .directive('messages', function() {
            return {
                restrict: 'E',
                template: 
                    '<div class="alert alert-success" ng-show="mensagens.sucesso.length > 0">' +
                    '    <ul>' +
                    '        <li ng-repeat="mensagem in mensagens.sucesso">{{mensagem}}</li>' +
                    '    </ul>' +
                    '</div>' +
                    '<div class="alert alert-info" ng-show="mensagens.informacao.length > 0">' +
                    '    <ul>' +
                    '        <li ng-repeat="mensagem in mensagens.informacao">{{mensagem}}</li>' +
                    '    </ul>' +
                    '</div>' +
                    '<div class="alert alert-warning" ng-show="mensagens.alerta.length > 0">' +
                    '    <ul>' +
                    '        <li ng-repeat="mensagem in mensagens.alerta">{{mensagem}}</li>' +
                    '    </ul>' +
                    '</div>' +
                    '<div class="alert alert-danger" ng-show="mensagens.erro.length > 0">' +
                    '    <ul>' +
                    '        <li ng-repeat="mensagem in mensagens.erro">{{mensagem}}</li>' +
                    '    </ul>' +
                    '</div>'
            };
        });
})();